import 'package:dartlearn/src/models/base_response.dart';
import 'package:dartlearn/src/json_demo/video_category.dart';
import 'package:dartlearn/src/net/request_manager.dart';

class VideoCategoryApi {
  static getVideoCategory() async {
    try {
      /// https://gank.io/special/Flutter
      // https://api.apiopen.top/videoCategory
      // https://api.apiopen.top/getJoke?page=1&count=2&type=video

      const url = 'https://api.apiopen.top/videoCategory';
      BaseResponse response = await RequestManager.instance.get(url);
      final VideoCategory videoCategory = VideoCategory.fromJson(response.data);
      print(videoCategory.count);
    } catch (e) {
      print(e);
    }
  }
}
