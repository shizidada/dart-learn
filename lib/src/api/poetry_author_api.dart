import 'dart:collection';

import 'package:dartlearn/src/models/author_info_list.dart';
import 'package:dartlearn/src/models/base_response.dart';
import 'package:dartlearn/src/net/request_manager.dart';

class PoetryAuthorApi {
  static getPoetryAuthorList() async {
    try {
      const url = 'http://localhost:7000/api/v1/poetry/author/list';
      Map<String, dynamic> params = HashMap();
      params['pageSize'] = 10;
      params['pageNum'] = 2;
      BaseResponse response = await RequestManager.instance.post(url, params: params);
      AuthorInfoList authorInfo = AuthorInfoList.fromJson(response.data);
      print(authorInfo.pageNum);
      print(authorInfo.pageSize);
      print(authorInfo.totalSize);
      authorInfo.list.forEach((element) {
        // Map<String, dynamic> json = element.toJson();
        print(element?.authorName);
      });
    } catch (e) {
      print(e);
    }
  }
}
