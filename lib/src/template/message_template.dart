class MessageTemplate {
  /// 消息类型
  String type;

  /// 聊天类型
  String chatType;

  /// 消息内容
  String content;

  String sendId;

  String receiveId;

  MessageTemplate(
      {this.type, this.chatType, this.sendId, this.receiveId, this.content});

  Map<String, dynamic> toJson() => _$THMessageTemplateToJson(this);

  Map<String, dynamic> _$THMessageTemplateToJson(MessageTemplate instance) =>
      <String, dynamic>{
        'type': instance.type,
        'chatType': instance.chatType,
        'sendId': instance.sendId,
        'receiveId': instance.receiveId,
        'content': instance.content,
      };
}
