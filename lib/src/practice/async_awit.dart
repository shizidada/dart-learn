import 'dart:async';

class Student {
  String name;

  String age;

  Student(this.age, this.name);

  @override
  String toString() {
    return 'Student { age: $age, name: $name}';
  }
}

Duration duration = Duration(seconds: 2);

Future<String> getName() => Future.delayed(duration, () => 'Tom');

Future<String> gatherNewsReports() => Future.delayed(duration, () => 'news');

Future<Student> getStudent() =>
    Future.delayed(duration, () => Student('16', "Tom"));

main(List<String> args) async {
  // String name = await getName();
  // print("Hello Async $name");

  // print(args);

  // print(Directory.current.path);

  // String files = File(Directory.current.path).readAsStringSync();
  // print(files);

  // Stream.fromFuture(getName()).listen((event) {
  //   print(event);
  // });

  // Stream.fromFuture(getStudent()).listen((event) {
  //   print(event);
  // });

  // Stream.empty().listen((value) {
  //   print('${value}');
  // }, onError: (obj, strace) {
  //   print('error');
  // }, onDone: () {
  //   print('done');
  // });

  int count = 0;
  StreamController controller = StreamController(onPause: () {
    print('pause');
  }, onResume: () {
    print('resume');
  }, onCancel: () {
    print('cancel');
  });

  Timer.periodic(Duration(seconds: 1), (value) {
    controller.sink.add(count++);
    print('add');
  });

  StreamSubscription subscription = controller.stream.listen((value) {
    print('$value');
  }, onError: (error) {
    print('$error');
  }, onDone: () {
    print('done');
  });

  // Future.delayed(Duration(seconds: 3), () {
  //   subscription.cancel();
  // });
  subscription.onData((value) {
    print('$value');
  });

  Future.delayed(Duration(seconds: 5), () {
    //暂定
    subscription.pause();
  });

  Future.delayed(Duration(seconds: 7), () {
    //恢复
    subscription.resume();
  });
}
