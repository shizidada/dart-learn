void main() {
  const double a = 12.3;
  const double b = a * 100;
  print(b);

  var pnum = num.parse("12");
  print(pnum);

  String piAsString = 3.14159.toStringAsFixed(2);
  print(piAsString);

  const msPerSecond = 1000;
  const secondsUntilRetry = 5;
  const msUntilRetry = secondsUntilRetry * msPerSecond;
  print(msUntilRetry);

  var s1 = 'Single quotes work well for string literals.';
  var s2 = "Double quotes work just as well.";
  var s3 = 'It\'s easy to escape the string delimiter.';
  var s4 = "It's even easier to use the other delimiter.";

  var s = 'string interpolation';

  assert('Dart has $s, which is very handy.' ==
      'Dart has string interpolation, ' + 'which is very handy.');
  assert('That deserves all caps. ' + '${s.toUpperCase()} is very handy!' ==
      'That deserves all caps. ' + 'STRING INTERPOLATION is very handy!');

  var s5 = '''You can create 
  multi-line strings like this one. ''';

  var s6 = """This is also a
                multi-line string.""";

  var s7 = r'In a raw string, not even \n gets special treatment.';
  print(s5);
  print(s6);
  print(s7);
  print("---------------------------------");

  bool isSuccess = false;
  print(isSuccess);

  var clapping = '\u{1f44f}';
  print(clapping);

  print(clapping.codeUnits);
  print(clapping.runes.toList());

  Runes input = new Runes(
      '\u2665  \u{1f605}  \u{1f60e}  \u{1f47b}  \u{1f596}  \u{1f44d}');
  print(new String.fromCharCodes(input));
}
