import 'dart:async';

class Student {
  Student(this.name, this.phone);

  String name;
  String phone;
}

main(List<String> args) {
  // Student st1 = new Student("ax", "12");
  // print(st1.name);

  const timeout = const Duration(seconds: 3);
  const ms = const Duration(milliseconds: 1);
  startTimeout([int milliseconds, callback]) {
    var duration = milliseconds == null ? timeout : ms * milliseconds;
    var handleTimeout = callback == null ? () {} : callback;
    return new Timer(duration, handleTimeout);
  }

  startTimeout(22, () {
    print("startTimeout");
  });

  Timer.run(() => {print('object')});

  Future.delayed(Duration(seconds: 3), () {
    Student st1 = new Student("ax", "12");
    print("delayed $st1");
  });

  // var map = new Map();
  // var map2 = {};
  // map2['name'] = "zs";
  // print(map2);

  // const items = [1, 2, 34];

  // for (var item in items) {
  //   print(item);
  // }
}
