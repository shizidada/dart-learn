import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:dartlearn/src/models/message_info.dart';
import 'package:web_socket_channel/io.dart';
// import 'package:web_socket_channel/status.dart' as status;

void main() async {
  Map<String, dynamic> headers = HashMap();
  headers['Authorization'] = 'Bearer 2794baf1-778b-4329-a8cb-d2514e3d913a';
  final _webSocketChannel = await IOWebSocketChannel.connect(
      'ws://127.0.0.1:7000/socket.io/801901011592810500',
      headers: headers);
  _sendMessage(String message) {
    print('$message');
    _webSocketChannel.sink.add(message);
  }

  Timer.periodic(Duration(seconds: 2), (timer) {
    MessageInfo info = MessageInfo(801901011592810500, "你吃饭了吗？？");
    _sendMessage(json.encode(info));
  });

  _socketChannelDataListen(message) {
    print('----------- on data ---------------- $message');
  }

  _socketChannelErrorListen(message) {
    print('----------- on error ---------------- $message');
  }

  _socketChannelDoneListen() {
    print('-------------- on done -------------');
  }

  _webSocketChannel.stream.listen(_socketChannelDataListen,
      onError: _socketChannelErrorListen, onDone: _socketChannelDoneListen);
}
