import 'dart:async';

import 'package:dartlearn/src/template/message_template.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';

main(List<String> args) {
  String toUserId = '775113183131074580';
  String currentUserId = '786600935907659776';
  IO.Socket channel = IO.io(
      'http://localhost:8090',
      OptionBuilder()
          // for Flutter or Dart VM
          .setTransports(['websocket'])
          // disable auto-connection
          .disableAutoConnect()
          // optional
          // .setExtraHeaders({'access_token': accessToken})
          .build());

  channel.connect();

  // 链接建立成功之后，可以发送数据到socket.io的后端了
  channel.on('connect', (data) => print('connect $data'));

  // 链接建立失败时调用
  channel.on('error', (data) => print('error $data'));

  // 链接出错时调用
  channel.on("connect_error", (data) => print('connect_error: $data'));

  // 链接断开时调用
  channel.on('disconnect', (data) => print('disconnect $data'));

  // 链接关闭时调用
  channel.on('close', (data) => print('close $data'));

  // 服务端 emit 一个 message 事件时，可以直接监听到
  channel.on('message', (data) => print('onmessage $data'));

  channel.on('SINGLE_CHAT', (data) => print('收到消息 $data'));

  Timer.periodic(Duration(seconds: 3), (timer) {
    MessageTemplate template = MessageTemplate(
        type: "MS:TEXT",
        chatType: "CT:SINGLE",
        sendId: currentUserId,
        receiveId: toUserId,
        content: "测试消息");
    channel.emit('SINGLE_CHAT', template.toJson());
  });
}
