class Singleton {
  Singleton._();

  static final _instance = Singleton._();

  factory Singleton.getInstance() => _instance;
}

class Collection {
  add() {
    print('add');
  }

  remove() {
    print("remove");
  }
}

class DBManager {
  factory DBManager() => _getInstance();
  static DBManager get instance => _getInstance();
  static DBManager _instance;

  Collection collection;

  DBManager._internal() {
    _initCollection();
  }

  static DBManager _getInstance() {
    if (_instance == null) {
      _instance = DBManager._internal();
    }
    return _instance;
  }

  Future<void> _initCollection() async {
    if (collection == null) {
      collection = Collection();
    }
  }
}

main(List<String> args) {
  // print(Singleton.getInstance().runtimeType);
  DBManager.instance.collection.add();
  DBManager.instance.collection.remove();
}
