void main() {
  var name = "tom";
  print(name);

  name = "pony";
  print(name);

  var number = 123;
  print(number);

  const password = "admin";
//  password = "admin123";
  print(password);

  final title = "This is title";
  print(title);

  dynamic desc = "what's you name ?";
  print(desc);

  dynamic price = 123;
  print(price);

  price = 12.5;
  print(price);

  int amount = 123;
  print(amount.toDouble());

  double count = 123.5;
  print(count.toInt());
  ///////////////////////////////////////
}
