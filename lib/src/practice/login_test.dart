import 'package:dartlearn/src/json_demo/access_token.dart';
import 'package:dartlearn/src/json_demo/account_info.dart';
import 'package:dio/dio.dart';

import '../models/base_response.dart';

main(List<String> args) {
  Dio dio = Dio();

  // final String login_url = "http://localhost:7000/api/v1/account/login";
  // Map<String, dynamic> params = Map();
  // params["accountName"] = "tom";
  // params["password"] = "123456";
  // params["loginType"] = "password";
  // dio.post(login_url, data: params).then((value) {
  //   BaseResponse response = BaseResponse.fromJson(value.data);
  //   // print(response.code);
  //   // print(response.message);
  //   // print(response.data);
  //   AccessToken token = AccessToken.fromJson(response.data);
  //   print(token.accessToken);
  //   print(token.refreshToken);
  // });

  final String info_url = "http://localhost:7000/api/v1/account/info";
  dio.post(info_url, queryParameters: {
    "access_token": '352e584d-0d83-4fa6-a78b-628c8d9f137a'
  }).then((value) {
    BaseResponse response = BaseResponse.fromJson(value.data);
    AccountInfo accountInfo = AccountInfo.fromJson(response.data);
    print(accountInfo.data);
  });
}
