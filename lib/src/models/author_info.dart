import 'package:json_annotation/json_annotation.dart';

part 'author_info.g.dart';

@JsonSerializable()
class AuthorInfo {
  String id;
  String authorName;
  String introduction;
  String magnumOpus;
  String dynasty;
  int collected;

  AuthorInfo(
      {this.id,
      this.authorName,
      this.introduction,
      this.magnumOpus,
      this.dynasty,
      this.collected});

  factory AuthorInfo.fromJson(Map<String, dynamic> json) =>
      _$AuthorInfoFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorInfoToJson(this);
}
