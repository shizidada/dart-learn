import 'package:json_annotation/json_annotation.dart';

import 'author_info.dart';

part 'author_info_list.g.dart';

@JsonSerializable()
class AuthorInfoList {
  int pageNum;

  int pageSize;

  int totalSize;

  List<AuthorInfo> list;

  AuthorInfoList({this.pageNum, this.pageSize, this.totalSize, this.list});

  factory AuthorInfoList.fromJson(Map<String, dynamic> json) =>
      _$AuthorInfoListFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorInfoListToJson(this);
}
