import 'package:json_annotation/json_annotation.dart';

part 'base_response.g.dart';

@JsonSerializable()
class BaseResponse {
  //后台返回的错误码
  int code;

  //返回的信息
  String message;

  //返回的数据，需要自己进行处理成自己想要的对象
  Map<String, dynamic> data;

  BaseResponse(this.code, this.message, this.data);

  factory BaseResponse.fromJson(Map<String, dynamic> json) =>
      _$BaseResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BaseResponseToJson(this);
}
