// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'author_info_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthorInfoList _$AuthorInfoListFromJson(Map<String, dynamic> json) {
  return AuthorInfoList(
    pageNum: json['pageNum'] as int,
    pageSize: json['pageSize'] as int,
    totalSize: json['totalSize'] as int,
    list: (json['list'] as List)
        ?.map((e) =>
            e == null ? null : AuthorInfo.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$AuthorInfoListToJson(AuthorInfoList instance) =>
    <String, dynamic>{
      'pageNum': instance.pageNum,
      'pageSize': instance.pageSize,
      'totalSize': instance.totalSize,
      'list': instance.list,
    };
