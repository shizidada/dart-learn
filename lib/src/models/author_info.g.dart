// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'author_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthorInfo _$AuthorInfoFromJson(Map<String, dynamic> json) {
  return AuthorInfo(
    id: json['id'] as String,
    authorName: json['authorName'] as String,
    introduction: json['introduction'] as String,
    magnumOpus: json['magnumOpus'] as String,
    dynasty: json['dynasty'] as String,
    collected: json['collected'] as int,
  );
}

Map<String, dynamic> _$AuthorInfoToJson(AuthorInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'authorName': instance.authorName,
      'introduction': instance.introduction,
      'magnumOpus': instance.magnumOpus,
      'dynasty': instance.dynasty,
      'collected': instance.collected,
    };
