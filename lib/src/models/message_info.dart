import 'package:json_annotation/json_annotation.dart';

part 'message_info.g.dart';

@JsonSerializable()
class MessageInfo {
  num toId;

  String message;

  MessageInfo(this.toId, this.message);

  factory MessageInfo.fromJson(Map<String, dynamic> json) =>
      _$MessageInfoFromJson(json);

  Map<String, dynamic> toJson() => _$MessageInfoToJson(this);
}
