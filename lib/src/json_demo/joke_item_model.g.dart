// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joke_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JokeItemModel _$JokeItemModelFromJson(Map<String, dynamic> json) {
  return JokeItemModel(
    json['sid'] as String,
    json['text'] as String,
    json['type'] as String,
    json['thumbnail'] as String,
    json['video'] as String,
    json['images'] as String,
    json['up'] as String,
    json['down'] as String,
    json['forward'] as String,
    json['comment'] as String,
    json['uid'] as String,
    json['name'] as String,
    json['header'] as String,
    json['top_comments_content'] as String,
    json['top_comments_voiceuri'] as String,
    json['top_comments_uid'] as String,
    json['top_comments_name'] as String,
    json['top_comments_header'] as String,
  )..passtime = json['passtime'] as String;
}

Map<String, dynamic> _$JokeItemModelToJson(JokeItemModel instance) =>
    <String, dynamic>{
      'sid': instance.sid,
      'text': instance.text,
      'type': instance.type,
      'thumbnail': instance.thumbnail,
      'video': instance.video,
      'images': instance.images,
      'up': instance.up,
      'down': instance.down,
      'forward': instance.forward,
      'comment': instance.comment,
      'uid': instance.uid,
      'name': instance.name,
      'header': instance.header,
      'top_comments_content': instance.top_comments_content,
      'top_comments_voiceuri': instance.top_comments_voiceuri,
      'top_comments_uid': instance.top_comments_uid,
      'top_comments_name': instance.top_comments_name,
      'top_comments_header': instance.top_comments_header,
      'passtime': instance.passtime,
    };
