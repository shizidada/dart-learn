// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountInfoItem _$AccountInfoItemFromJson(Map<String, dynamic> json) {
  return AccountInfoItem(
    json['userId'] as int,
    json['userName'] as String,
    json['avatar'] as String,
    json['address'] as String,
    json['description'] as String,
    json['email'] as String,
    json['gender'] as String,
    json['job'] as String,
    json['phone'] as String,
  );
}

Map<String, dynamic> _$AccountInfoItemToJson(AccountInfoItem instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'userName': instance.userName,
      'avatar': instance.avatar,
      'gender': instance.gender,
      'description': instance.description,
      'email': instance.email,
      'phone': instance.phone,
      'job': instance.job,
      'address': instance.address,
    };

AccountInfo _$AccountInfoFromJson(Map<String, dynamic> json) {
  return AccountInfo(
    json['code'] as int,
    json['message'] as String,
  )..data = json['data'] == null
      ? null
      : AccountInfoItem.fromJson(json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AccountInfoToJson(AccountInfo instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };
