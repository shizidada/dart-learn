import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';
import 'joke_item_model.dart';

part 'joke_model.g.dart';

@JsonSerializable()
class JokeModel extends BaseResponse {
  List<JokeItemModel> result;

  JokeModel(int code, String message) : super(code, message) {
    this.result = result;
  }

  factory JokeModel.fromJson(Map<String, dynamic> json) =>
      _$JokeModelFromJson(json);

  Map<String, dynamic> toJson() => _$JokeModelToJson(this);
}
