// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_category_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoCategoryItem _$VideoCategoryItemFromJson(Map<String, dynamic> json) {
  return VideoCategoryItem(
    json['adIndex'] as int,
    json['tag'],
    json['id'] as int,
    json['type'] as String,
  )..data = json['data'] == null
      ? null
      : CategoryItem.fromJson(json['data'] as Map<String, dynamic>);
}

Map<String, dynamic> _$VideoCategoryItemToJson(VideoCategoryItem instance) =>
    <String, dynamic>{
      'adIndex': instance.adIndex,
      'tag': instance.tag,
      'id': instance.id,
      'type': instance.type,
      'data': instance.data,
    };
