import 'package:json_annotation/json_annotation.dart';

part 'joke_item_model.g.dart';

@JsonSerializable()
class JokeItemModel {
  /**
   *  sid: "31577089",
   *  text: "孩子厉害👍",
   *  type: "video",
   *  thumbnail: "http://wimg.spriteapp.cn/picture/2020/1026/5f967bc4e7de2_wpd.jpg",
   *  video: "http://uvideo.spriteapp.cn/video/2020/1026/5f967bc4e7de2_wpd.mp4",
   *  images: null,
   *  up: "114",
   *  down: "3",
   *  forward: "0",
   *  comment: "6",
   *  uid: "23005857",
   *  name: "无情无义",
   *  header: "http://wimg.spriteapp.cn/profile/large/2020/02/09/5e3fc8f551f9a_mini.jpg",
   *  top_comments_content: "厉害不厉害不知道。反正比我强",
   *  top_comments_voiceuri: "",
   *  top_comments_uid: "11981984",
   *  top_comments_name: "不得姐用户",
   *  top_comments_header: "http://qzapp.qlogo.cn/qzapp/100336987/D2C67A061C37841FD39E2D6232DE9833/100",
   *  passtime: "2020-12-24 18:30:05"
   */
  String sid;
  String text;
  String type;
  String thumbnail;
  String video;
  String images;
  String up;
  String down;
  String forward;
  String comment;
  String uid;
  String name;
  String header;
  String top_comments_content;
  String top_comments_voiceuri;
  String top_comments_uid;
  String top_comments_name;
  String top_comments_header;
  String passtime;
  JokeItemModel(
    this.sid,
    this.text,
    this.type,
    this.thumbnail,
    this.video,
    this.images,
    this.up,
    this.down,
    this.forward,
    this.comment,
    this.uid,
    this.name,
    this.header,
    this.top_comments_content,
    this.top_comments_voiceuri,
    this.top_comments_uid,
    this.top_comments_name,
    this.top_comments_header,
  );

  factory JokeItemModel.fromJson(Map<String, dynamic> json) =>
      _$JokeItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$JokeItemModelToJson(this);
}
