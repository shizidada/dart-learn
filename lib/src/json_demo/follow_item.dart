import 'package:json_annotation/json_annotation.dart';

part 'follow_item.g.dart';

@JsonSerializable()
class FollowItem {
  int itemId;
  String itemType;
  bool followed;

  FollowItem(this.followed, this.itemId, this.itemType);

  factory FollowItem.fromJson(Map<String, dynamic> json) =>
      _$FollowItemFromJson(json);

  Map<String, dynamic> toJson() => _$FollowItemToJson(this);
}
