// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'follow_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FollowItem _$FollowItemFromJson(Map<String, dynamic> json) {
  return FollowItem(
    json['followed'] as bool,
    json['itemId'] as int,
    json['itemType'] as String,
  );
}

Map<String, dynamic> _$FollowItemToJson(FollowItem instance) =>
    <String, dynamic>{
      'itemId': instance.itemId,
      'itemType': instance.itemType,
      'followed': instance.followed,
    };
