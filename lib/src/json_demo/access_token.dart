import 'package:dartlearn/src/json_demo/base_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'access_token.g.dart';

@JsonSerializable()
class AccessToken extends BaseResponse {
  String data;

  AccessToken(int code, message) : super(code, message) {
    this.data = data;
  }

  factory AccessToken.fromJson(Map<String, dynamic> json) =>
      _$AccessTokenFromJson(json);

  Map<String, dynamic> toJson() => _$AccessTokenToJson(this);
}
