// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joke_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JokeModel _$JokeModelFromJson(Map<String, dynamic> json) {
  return JokeModel(
    json['code'] as int,
    json['message'] as String,
  )..result = (json['result'] as List)
      ?.map((e) =>
          e == null ? null : JokeItemModel.fromJson(e as Map<String, dynamic>))
      ?.toList();
}

Map<String, dynamic> _$JokeModelToJson(JokeModel instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'result': instance.result,
    };
