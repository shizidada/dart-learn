import 'package:dartlearn/src/json_demo/base_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'account_info.g.dart';

@JsonSerializable()
class AccountInfoItem {
  int userId;
  String userName;
  String avatar;
  String gender;
  String description;
  String email;
  String phone;
  String job;
  String address;

  AccountInfoItem(this.userId, this.userName, this.avatar, this.address,
      this.description, this.email, this.gender, this.job, this.phone);

  factory AccountInfoItem.fromJson(Map<String, dynamic> json) =>
      _$AccountInfoItemFromJson(json);

  Map<String, dynamic> toJson() => _$AccountInfoItemToJson(this);
}

@JsonSerializable()
class AccountInfo extends BaseResponse {
  AccountInfoItem data;

  AccountInfo(
    int code,
    String message,
  ) : super(code, message);

  factory AccountInfo.fromJson(Map<String, dynamic> json) =>
      _$AccountInfoFromJson(json);

  Map<String, dynamic> toJson() => _$AccountInfoToJson(this);
}
