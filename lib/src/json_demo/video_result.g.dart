// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoResult _$VideoResultFromJson(Map<String, dynamic> json) {
  return VideoResult(
    json['code'] as int,
    json['message'] as String,
    json['result'] == null
        ? null
        : VideoCategory.fromJson(json['result'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$VideoResultToJson(VideoResult instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'result': instance.result,
    };
