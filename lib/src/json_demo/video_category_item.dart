import 'package:dartlearn/src/json_demo/category_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'video_category_item.g.dart';

@JsonSerializable()
class VideoCategoryItem {
  int adIndex;
  dynamic tag;
  int id;
  String type;
  CategoryItem data;

  VideoCategoryItem(this.adIndex, this.tag, this.id, this.type);

  factory VideoCategoryItem.fromJson(Map<String, dynamic> json) =>
      _$VideoCategoryItemFromJson(json);

  Map<String, dynamic> toJson() => _$VideoCategoryItemToJson(this);
}
