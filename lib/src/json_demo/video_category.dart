import 'package:dartlearn/src/json_demo/video_category_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'video_category.g.dart';

@JsonSerializable()
class VideoCategory {
  bool adExist;
  int total;
  String nextPageUrl;
  int count;
  List<VideoCategoryItem> itemList;

  VideoCategory(
      this.adExist, this.count, this.nextPageUrl, this.total, this.itemList);

  factory VideoCategory.fromJson(Map<String, dynamic> json) =>
      _$VideoCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$VideoCategoryToJson(this);
}
