import 'package:json_annotation/json_annotation.dart';
import 'package:dartlearn/src/json_demo/follow_item.dart';

part 'category_item.g.dart';

@JsonSerializable()
class CategoryItem {
  bool ifShowNotificationIcon;
  bool expert;
  String dataType;
  String icon;
  String actionUrl;
  String description;
  String title;
  int uid;
  String subTitle;
  String iconType;
  bool ifPgc;
  int id;
  String adTrack;
  FollowItem follow;

  CategoryItem(
      this.actionUrl,
      this.adTrack,
      this.dataType,
      this.description,
      this.expert,
      this.icon,
      this.iconType,
      this.id,
      this.ifPgc,
      this.ifShowNotificationIcon,
      this.subTitle,
      this.title,
      this.uid,
      this.follow);
  factory CategoryItem.fromJson(Map<String, dynamic> json) =>
      _$CategoryItemFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryItemToJson(this);
}
