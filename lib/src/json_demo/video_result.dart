import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';
import 'video_category.dart';

part 'video_result.g.dart';

@JsonSerializable()
class VideoResult extends BaseResponse {
  VideoCategory result;

  VideoResult(int code, String message, this.result) : super(code, message);

  factory VideoResult.fromJson(Map<String, dynamic> json) =>
      _$VideoResultFromJson(json);

  Map<String, dynamic> toJson() => _$VideoResultToJson(this);
}
