// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoCategory _$VideoCategoryFromJson(Map<String, dynamic> json) {
  return VideoCategory(
    json['adExist'] as bool,
    json['count'] as int,
    json['nextPageUrl'] as String,
    json['total'] as int,
    (json['itemList'] as List)
        ?.map((e) => e == null
            ? null
            : VideoCategoryItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$VideoCategoryToJson(VideoCategory instance) =>
    <String, dynamic>{
      'adExist': instance.adExist,
      'total': instance.total,
      'nextPageUrl': instance.nextPageUrl,
      'count': instance.count,
      'itemList': instance.itemList,
    };
