import 'package:dartlearn/src/json_demo/base_response.dart';
import 'package:dio/dio.dart';

import 'access_token.dart';
import 'account_info.dart';
import 'video_result.dart';

main(List<String> args) async {
  try {
    // const url = "https://api.apiopen.top/getJoke?page=1&count=2&type=video";
    // const url = 'https://api.apiopen.top/videoCategory';
    Dio dio = Dio();

    final String loginUrl = "http://localhost:7000/api/v1/account/login";
    Map<String, dynamic> params = Map();
    params["accountName"] = "李白";
    params["password"] = "123456";
    params["loginType"] = "password";
    Response response = await dio.post(loginUrl, queryParameters: params);
    print(response);

    final String info_url = "http://localhost:7000/api/v1/user/info";
    dio.post(info_url, queryParameters: {
      "access_token": 'ade59299-5833-496b-b5ab-42a8e3a84af1'
    }).then((response) {
      AccountInfo accountInfo = AccountInfo.fromJson(response.data);
      print(accountInfo.data.toJson());
    });
  } catch (e) {
    print(e);
  }
}

// JokeModel jokeModel = JokeModel.fromJson(response.data);
// for (var item in jokeModel.result) {
//   print(item.text);
// }
