// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryItem _$CategoryItemFromJson(Map<String, dynamic> json) {
  return CategoryItem(
    json['actionUrl'] as String,
    json['adTrack'] as String,
    json['dataType'] as String,
    json['description'] as String,
    json['expert'] as bool,
    json['icon'] as String,
    json['iconType'] as String,
    json['id'] as int,
    json['ifPgc'] as bool,
    json['ifShowNotificationIcon'] as bool,
    json['subTitle'] as String,
    json['title'] as String,
    json['uid'] as int,
    json['follow'] == null
        ? null
        : FollowItem.fromJson(json['follow'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CategoryItemToJson(CategoryItem instance) =>
    <String, dynamic>{
      'ifShowNotificationIcon': instance.ifShowNotificationIcon,
      'expert': instance.expert,
      'dataType': instance.dataType,
      'icon': instance.icon,
      'actionUrl': instance.actionUrl,
      'description': instance.description,
      'title': instance.title,
      'uid': instance.uid,
      'subTitle': instance.subTitle,
      'iconType': instance.iconType,
      'ifPgc': instance.ifPgc,
      'id': instance.id,
      'adTrack': instance.adTrack,
      'follow': instance.follow,
    };
